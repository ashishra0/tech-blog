+++
title = "Backup / restore Postgres data on Docker"
description = ""
tags = [
    "postgres",
    "docker",
    "pg",
    "development",
]
date = "2020-01-10"
categories = [
    "Development",
    "Postgres",
    "Docker"
]
+++

Postgres comes with handy tools such as **pg\_dump** and **psql** which
lets us take backups and restore them with ease.

To backup data, we can use `pg_dump`:

```bash
docker exec <postgres_container_name> pg_dump -U postgres <database_name> > backup.sql
```

This command will create a text file named `backup.sql` containing all
the data and schema of your database.

To import the data back into Postgres, we use `psql`:

```bash
cat backup.sql | docker exec -i <postgres_container_name> psql -U postgres -d <database_name> < backup.sql
```
> Note:
The above command assumes you have postgres as the default user
