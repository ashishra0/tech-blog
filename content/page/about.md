+++
title = "About Me"
+++

Hi. I'm Ashish, a software developer from Bangalore, India.

Work Update: I am currently a SDE Intern at [Postman](www.postman.com).

I've always been fascinated by how we use technology and interact with things. I am passionate about building amazing products backed by readily deployable high quality software.

---

## What I care about
* good mentorship
* continuous improvement + fun time while at work
* Making a meaningful contribution

---

## What I work on

I am a polyglot developer at heart with special interest in Ruby and Go.
I also write blog posts and contribute to open source projects including:

* [graphql-engine](https://github.com/hasura/graphql-engine)
* [Ruby Faker](https://github.com/faker-ruby/faker)
* [Faker.js](https://github.com/Marak/faker.js)

---

## Freelance

I have a diverse set of skills ranging from building websites with JAM stack to building mobile apps with React Native and Flutter. If any of the above is exactly what you are looking for then we should get in touch :)

Just shoot me an email here: ashishrao2598@gmail.com

---

<!-- Also, I'm open for hire. -->
You can find my [resume](https://drive.google.com/file/d/1yh4g5ZOCfsn6wGIreXvan2AKxDrmaPiv/view?usp=sharing) here. 🙌🏻