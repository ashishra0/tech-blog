+++
title = "Open Source Software My Story"
tags = [
    "postgres",
    "features",
    "search",
    "development",
]
categories = [
    "Development",
    "Database",
    "Postgres"
]
+++

![Header-image ><](https://images.unsplash.com/photo-1499084732479-de2c02d45fcc?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1489&q=80)
*Photo by [Benjamin Davies](https://unsplash.com/@bendavisual) on Unsplash*

> Contributing to open source is very difficult and the people who do contribute are experts with immense knowledge far beyond what I can fathom. I am just a beginner — it’s not meant for me.

Such was my state of mind whenever I decided to contribute to OSS. Call it lack of motivation or whatever, I just couldn’t get around contributing to OSS. Until one fine day I came across this video and the lessons it taught me to just have a go! I recommend everyone to watch this before reading ahead.

[![video-url vv](https://res.cloudinary.com/marcomontalbano/image/upload/v1583428402/video_to_markdown/images/youtube--iG9CE55wbtY-c05b58ac6eb4c4700831b2b3070cd403.jpg)](https://youtu.be/iG9CE55wbtY "")

Now in retrospect, it was indeed my fear of being wrong that was the root cause. I finally admitted that fact to myself and decided to be fearless.

Let’s back up a bit to give some context about my background.

I started three years back when I was introduced to programming with Python. Since then, I have been writing code in Ruby, Javascript, Go & SQL. Surprise, Surprise, I am NOT a Computer Science grad. I am just a person who enjoy’s writing code, building cool software. I am working on making a career out of this.

## So when did I first start contributing?

Following that day I saw that video on YouTube, I just logged on to GitHub and started looking around for some projects which seemed interesting and novice enough for me to start. Around September 2018, there was quite a lot of buzz around something called as Hacktoberfest. Create 5 Pull Requests and win a T-shirt. This was written on the website which quickly made me think I want that T-shirt. I think this was enough motivation I needed at the time to get going.

So I started searching what is a Pull Request? How to create one? Saw some quick videos on YouTube, but my original thoughts came back to haunt me. I was once again on the same path I found myself as to where to begin? Luckily there was a organisation called as FreeCodeCamp, Which had this amazing repository aimed at beginners to help them create their first PR. The repo was not about finding issues and then coming up with possible solution to resolve it, rather it was a curated list of articles or notes on tech topics for eg, A quick guide on Gatsby, A quick intro to Docker, etc. Every newbie like me at the time would need to write an article add few interesting pointers on the tech we came across so that other future readers can profit from it in terms of learning.

This was something I liked and since there were no complex issues I started finding topics that weren’t listed on the repository and quickly started writing about it and created PR as soon as I was finished writing. Gradually I got a hang of it and I had totally forgotten about Hacktoberfest and just went on writing articles and creating PR’s. I eventually got the T-shirt even though I was least expecting it.

This mentality of expecting some prize at the end of contributing to open source shouldn’t be there in the first place. Start contributing because it helps the community, start contributing because it helps your understanding of whatever the programming language or the tech it is related to, start contributing because at the end of the day you would have evolved as a developer.

## Where to start?

There is this amazing website that goes by the name of [CodeTriage](https://codetriage.com). You will find a list of repositories in your favoured programming language. By signing up on CodeTriage, you will get an email containing list of issues from the repo you have chosen.

Once you find a repository you wish to contribute, go to issues and under label search for some labels such as [good-first-issue] or [beginner]. These are specifically aimed at newbies and it will help you great deal.

I started to finally start contributing to OSS with the help of CodeTriage. I found a repo called Faker which was written in Ruby. This was it, I had this urge to face my fears and finally start writing code rather than writing articles as a contribution. I found few issues as [feature-request] and saw that nobody was working on it, so I decided to work on it. It took me a while to completely understand the codebase because it’s not just one or two files, there was a lot happening. It wasn’t irritating for me as how I thought it would be mainly because I was stoked on the idea that your code could be part of this codebase and many people would use it, this alone should serve as the biggest motivation rather than envisioning winning a T-shirt. My PR was approved and accepted by the maintainers and few other PR’s are still under revision, never ever have I felt so good about something.

![github-pr ><](/static/github_first_pr.png)

This is what I was talking about feels much better than receiving T-shirt, more and more reason to keep contributing because eventually it’s the community who are benefiting from it. 🎉

## Final Notes

This is how the process of contributing to OSS looks like:

* Find an issue to work on
* Study the codebase and understand what needs to be done
* Code. Ask questions on the issue. Commit
* Submit a PR
* Resolve review comments
* Go back to step 1

To all the people who are reading this, Programming is not hard, Contributing is definitely not hard. Even though having a good knowledge of programming language is not a must, as you can always help with the documentation or you can report any bugs you may have encountered and can always learn things along the way. You have to start somewhere right? 😉