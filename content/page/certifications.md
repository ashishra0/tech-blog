+++
title = "Certifications"
+++

**From Data structures & algorithms to Golang, React and many more.**

* [Algorithms + Data Structures](https://www.udemy.com/course/coding-interview-bootcamp-algorithms-and-data-structure/)

![DS_ALGOs ll](/static/go-certificate.jpg)

* [Go: The Complete Developer's Guide](https://www.udemy.com/course/go-the-complete-developers-guide/)

![go-image ll](/static/go_certificate_1.jpg)

* [React](https://reactforbeginners.com/)

![react-image ll](/static/react-certificate.png)
